package com.sabbir.android.apps.germanycoronainfo.ui.charts

import android.graphics.Color
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.utils.ColorTemplate
import com.sabbir.android.apps.germanycoronainfo.api.ApiUtils
import com.sabbir.android.apps.germanycoronainfo.models.DayWiseModel
import com.sabbir.android.apps.germanycoronainfo.models.GermanyLatestInfoSummary
import kotlinx.coroutines.launch


class ChartsViewModel : ViewModel() {


    val MATERIAL_COLORS = intArrayOf(
        ColorTemplate.rgb("#2ecc71"),
        ColorTemplate.rgb("#f1c40f"),
        ColorTemplate.rgb("#e74c3c"),
        ColorTemplate.rgb("#3498db")
    )


    private val apiService = ApiUtils.getApiService()
    private val casesData = MutableLiveData<List<DayWiseModel>>().apply {
        viewModelScope.launch {
            value = apiService.getDayWiseCases(7).caseList
        }
    }

    private val germanyLatestInfoSummary = MutableLiveData<GermanyLatestInfoSummary>().apply {
        viewModelScope.launch {
            value = apiService.getLatestInfoSummaryForGermany()
        }
    }
    val germanyLatestInfoSummaryObj: LiveData<GermanyLatestInfoSummary> = germanyLatestInfoSummary

    val casesDataList: LiveData<List<DayWiseModel>> = casesData

    private val deathDataForGivenPeriod = MutableLiveData<List<DayWiseModel>>().apply {
        viewModelScope.launch {
            value = apiService.getDayWiseCases(7).caseList
        }
    }

    val deathDataList: LiveData<List<DayWiseModel>> = deathDataForGivenPeriod

    fun generateBarChartData(dataList: List<DayWiseModel>): BarData {
        val barDataSet: MutableList<IBarDataSet> = mutableListOf()

        val entries: ArrayList<BarEntry> = ArrayList()

        for (j in dataList.indices) {
            val f = BarEntry(
                j.toFloat(),
                dataList[j].numberOfCases.toFloat()
            )
            entries.add(f)
        }

        val ds = BarDataSet(entries, "Number of Cases per day")
        ds.setColors(*ColorTemplate.VORDIPLOM_COLORS)
        barDataSet.add(ds)

        return BarData(barDataSet)
    }

    fun generatePieChartTotalData(germanyLatestInfoSummary: GermanyLatestInfoSummary): PieData {
        val count = 4

        val entries1: ArrayList<PieEntry> = ArrayList()

        entries1.add(PieEntry(germanyLatestInfoSummary.recovered.toFloat(), "Recovered"))
        entries1.add(PieEntry(germanyLatestInfoSummary.cases.toFloat(), "Cases"))
        entries1.add(PieEntry(germanyLatestInfoSummary.deaths.toFloat(), "Deaths"))

        val ds1 = PieDataSet(entries1, "Covid data")
        ds1.setColors(*ColorTemplate.MATERIAL_COLORS)
        ds1.sliceSpace = 2f
        ds1.valueTextColor = Color.WHITE
        ds1.valueTextSize = 12f

        return PieData(ds1)
    }


}