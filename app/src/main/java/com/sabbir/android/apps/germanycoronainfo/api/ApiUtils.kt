package com.sabbir.android.apps.germanycoronainfo.api

import com.sabbir.android.apps.germanycoronainfo.utils.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Sabbir Ahmed Himel aka himelhimu on 6/14/21 in Dhaka,Bangladesh.
 * @author himelhimu
 * sahimelhimu@gmail.com
 * This code is free of any charge or licencse or anything
 * do whatever you want.
 *
 */
object ApiUtils {

    fun getApiService(): ApiServices {
        return Retrofit.Builder()
            .apply {
                client(ApiProvider.getOkHttpClient())
                baseUrl(BASE_URL)
                addConverterFactory(GsonConverterFactory.create())
            }.build().create(ApiServices::class.java)
    }

}