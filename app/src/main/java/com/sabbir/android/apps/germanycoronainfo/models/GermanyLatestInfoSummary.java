
package com.sabbir.android.apps.germanycoronainfo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class GermanyLatestInfoSummary {

    @SerializedName("cases")
    @Expose
    private Integer cases;
    @SerializedName("deaths")
    @Expose
    private Integer deaths;
    @SerializedName("recovered")
    @Expose
    private Integer recovered;
    @SerializedName("weekIncidence")
    @Expose
    private Double weekIncidence;
    @SerializedName("casesPer100k")
    @Expose
    private Double casesPer100k;
    @SerializedName("casesPerWeek")
    @Expose
    private Integer casesPerWeek;
    @SerializedName("delta")
    @Expose
    private Delta delta;
    @SerializedName("r")
    @Expose
    private R r;
    @SerializedName("meta")
    @Expose
    private Meta meta;

    public Integer getCases() {
        return cases;
    }

    public void setCases(Integer cases) {
        this.cases = cases;
    }

    public Integer getDeaths() {
        return deaths;
    }

    public void setDeaths(Integer deaths) {
        this.deaths = deaths;
    }

    public Integer getRecovered() {
        return recovered;
    }

    public void setRecovered(Integer recovered) {
        this.recovered = recovered;
    }

    public Double getWeekIncidence() {
        return weekIncidence;
    }

    public void setWeekIncidence(Double weekIncidence) {
        this.weekIncidence = weekIncidence;
    }

    public Double getCasesPer100k() {
        return casesPer100k;
    }

    public void setCasesPer100k(Double casesPer100k) {
        this.casesPer100k = casesPer100k;
    }

    public Integer getCasesPerWeek() {
        return casesPerWeek;
    }

    public void setCasesPerWeek(Integer casesPerWeek) {
        this.casesPerWeek = casesPerWeek;
    }

    public Delta getDelta() {
        return delta;
    }

    public void setDelta(Delta delta) {
        this.delta = delta;
    }

    public R getR() {
        return r;
    }

    public void setR(R r) {
        this.r = r;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public String toString() {
        return "GermanyLatestInfoSummary{" +
                "cases=" + cases +
                ", deaths=" + deaths +
                ", recovered=" + recovered +
                ", weekIncidence=" + weekIncidence +
                ", casesPer100k=" + casesPer100k +
                ", casesPerWeek=" + casesPerWeek +
                ", delta=" + delta +
                ", r=" + r +
                ", meta=" + meta +
                '}';
    }
}
