package com.sabbir.android.apps.germanycoronainfo.models

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Sabbir Ahmed Himel aka 'bohemianrobot' on 6/18/21 in Dhaka,Bangladesh.
 * @author bohemianrobot
 * sahimelhimu@gmail.com
 * This code is free of any charge or license or anything
 * do whatever you want.
 *
 */
data class DayWiseModel(
    @SerializedName("cases")
    @Expose
    val numberOfCases: Int,
    @SerializedName("date")
    @Expose
    val date: String
):Serializable
