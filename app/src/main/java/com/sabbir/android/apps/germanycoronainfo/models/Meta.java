
package com.sabbir.android.apps.germanycoronainfo.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Meta {

    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("info")
    @Expose
    private String info;
    @SerializedName("lastUpdate")
    @Expose
    private String lastUpdate;
    @SerializedName("lastCheckedForUpdate")
    @Expose
    private String lastCheckedForUpdate;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getLastCheckedForUpdate() {
        return lastCheckedForUpdate;
    }

    public void setLastCheckedForUpdate(String lastCheckedForUpdate) {
        this.lastCheckedForUpdate = lastCheckedForUpdate;
    }

}
