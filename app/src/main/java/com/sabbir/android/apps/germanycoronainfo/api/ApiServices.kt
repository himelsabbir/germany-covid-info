package com.sabbir.android.apps.germanycoronainfo.api

import android.media.Image
import com.sabbir.android.apps.germanycoronainfo.models.DayWiseModel
import com.sabbir.android.apps.germanycoronainfo.models.GermanyLatestInfoSummary
import com.sabbir.android.apps.germanycoronainfo.models.HistoryResponseModel
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Sabbir Ahmed Himel aka himelhimu on 6/14/21 in Dhaka,Bangladesh.
 * @author himelhimu
 * sahimelhimu@gmail.com
 * This code is free of any charge or licencse or anything
 * do whatever you want.
 *
 */
interface ApiServices {

    @GET("germany")
    suspend fun getLatestInfoSummaryForGermany(): GermanyLatestInfoSummary

    @GET("germany/history/cases")
    suspend fun getAllHistory()

    @GET("germany/history/cases/{numberOfDays}")
    suspend fun getDayWiseCases(@Path("numberOfDays") numberOfDays:Int):HistoryResponseModel

    @GET("germany/history/deaths/{numberOfDays}")
    suspend fun getDayWiseDeaths(@Field("numberOfDays") numberOfDays:Int)

    @GET("germany/history/recovered/{numberOfDays}")
    suspend fun getDayWiseRecovered(@Field("numberOfDays") numberOfDays:Int)

    @GET("germany/age-groups")
    suspend fun getAgeGroupData()

    /* Returns a png heatmap of the districts
    * */
    @GET("map/districts")
    suspend fun getDistrictIncidentHeatMap():ResponseBody


}