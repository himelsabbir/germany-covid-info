package com.sabbir.android.apps.germanycoronainfo.ui.home

import android.app.AlertDialog
import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.sabbir.android.apps.germanycoronainfo.databinding.FragmentHomeBinding
import com.sabbir.android.apps.germanycoronainfo.utils.Helper
import dmax.dialog.SpotsDialog
import java.util.*

private const val TAG = "HomeFragment"

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null
    lateinit var dialog: AlertDialog

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)
        dialog = SpotsDialog.Builder().setContext(context).build()
        if (Helper.isConnectedToInternet(requireContext())) {
            dialog.show()
        } else Toast.makeText(context, "Internet Connection is required", Toast.LENGTH_LONG).show()

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        homeViewModel.germanyLatestInfoSummaryObj.observe(viewLifecycleOwner, {
            Log.d(TAG, "onCreateView: $it")
            dialog.dismiss()
            binding.newDeath.text = it.delta.deaths.toString()
            binding.totalCases.text = it.cases.toString()
            binding.totalDeath.text = it.deaths.toString()
            binding.totalRecover.text = it.recovered.toString()
            binding.newRecovered.text = it.delta.recovered.toString()
            binding.newCases.text = it.delta.cases.toString()
        })

        return binding.root
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sdf = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
        val currentDate = sdf.format(Date())
        binding.date.text = currentDate
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}