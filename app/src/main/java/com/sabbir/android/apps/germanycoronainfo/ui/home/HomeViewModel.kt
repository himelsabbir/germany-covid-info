package com.sabbir.android.apps.germanycoronainfo.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sabbir.android.apps.germanycoronainfo.api.ApiUtils
import com.sabbir.android.apps.germanycoronainfo.models.GermanyLatestInfoSummary
import com.sabbir.android.apps.germanycoronainfo.utils.Helper
import kotlinx.coroutines.launch

class HomeViewModel : ViewModel() {

    private val apiService = ApiUtils.getApiService()
    private val germanyLatestInfoSummary = MutableLiveData<GermanyLatestInfoSummary>().apply {
        viewModelScope.launch {
            value = apiService.getLatestInfoSummaryForGermany()
        }
    }
    val germanyLatestInfoSummaryObj: LiveData<GermanyLatestInfoSummary> = germanyLatestInfoSummary
}