package com.sabbir.android.apps.germanycoronainfo.ui.dashboard

import android.app.AlertDialog
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.sabbir.android.apps.germanycoronainfo.R
import com.sabbir.android.apps.germanycoronainfo.databinding.FragmentDashboardBinding
import com.sabbir.android.apps.germanycoronainfo.models.DayWiseModel
import com.sabbir.android.apps.germanycoronainfo.ui.charts.ChartsViewModel
import com.sabbir.android.apps.germanycoronainfo.utils.Helper
import dmax.dialog.SpotsDialog
import java.util.stream.Collectors

class DashboardFragment : Fragment() {

    private lateinit var chartsViewModel: ChartsViewModel
    private lateinit var dashboardViewModel: DashboardViewModel
    private var _binding: FragmentDashboardBinding? = null
    lateinit var dialog: AlertDialog
    private lateinit var typeFaceOpenSansLight: Typeface

    private fun setUpDeathBarChart() {

        binding.barChart.description.isEnabled = false
        binding.barChart.setDrawGridBackground(false)
        binding.barChart.setDrawBarShadow(false)

        binding.barChart.legend.typeface = typeFaceOpenSansLight


        val leftAxis: YAxis = binding.barChart.axisLeft
        leftAxis.typeface = typeFaceOpenSansLight
        leftAxis.axisMinimum = 0f // this replaces setStartAtZero(true)


        binding.barChart.axisRight.isEnabled = false

        /*val xAxis: XAxis = barChart.xAxis
        xAxis.isEnabled = false*/


        binding.barChart.xAxis.position = XAxis.XAxisPosition.BOTTOM
    }

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        chartsViewModel =
            ViewModelProvider(this).get(ChartsViewModel::class.java)
        dashboardViewModel =
            ViewModelProvider(this).get(DashboardViewModel::class.java)
        dialog = SpotsDialog.Builder().setContext(context).build()
        typeFaceOpenSansLight = Typeface.createFromAsset(context?.assets, "OpenSans-Light.ttf")

        if (Helper.isConnectedToInternet(requireContext())) {
            dialog.show()
        } else Toast.makeText(context, "Internet Connection is required", Toast.LENGTH_LONG).show()
        _binding = FragmentDashboardBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.radiogroupoptions.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.last_7_days -> {
                    dialog.show()
                    binding.currentDaysTv.text = String.
                    format(getString(R.string.showing_new_cases_for_the_last_d_days),7)
                    dashboardViewModel.getHistoryData(7)}
                R.id.last_15_days -> {
                    dialog.show()
                    binding.currentDaysTv.text = String.
                    format(getString(R.string.showing_new_cases_for_the_last_d_days),15)
                    dashboardViewModel.getHistoryData(15)}
            }
        }

        dashboardViewModel.germanyLatestInfoSummary.observe(viewLifecycleOwner, {
            dialog.dismiss()
            setUpDeathBarChart()
            binding.barChart.data = chartsViewModel.generateBarChartData(it)
            val xAxisLabels: List<String> = it.stream().map { d ->
                Helper.parseOnlyDate(d.date)
            }.collect(Collectors.toList())
            binding.barChart.xAxis.valueFormatter = IndexAxisValueFormatter(xAxisLabels)
            binding.barChart.invalidate()
        })
        return root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}