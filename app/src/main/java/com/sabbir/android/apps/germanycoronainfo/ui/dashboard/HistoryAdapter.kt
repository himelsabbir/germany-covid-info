package com.sabbir.android.apps.germanycoronainfo.ui.dashboard

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.sabbir.android.apps.germanycoronainfo.R
import com.sabbir.android.apps.germanycoronainfo.databinding.HistoryItemBinding
import com.sabbir.android.apps.germanycoronainfo.models.DayWiseModel
import com.sabbir.android.apps.germanycoronainfo.utils.Helper

/**
 * Created by Sabbir Ahmed Himel aka 'bohemianrobot' on 6/18/21 in Dhaka,Bangladesh.
 * @author bohemianrobot
 * sahimelhimu@gmail.com
 * This code is free of any charge or license or anything
 * do whatever you want.
 *
 */
class HistoryAdapter(private val dataSet: List<DayWiseModel>) : RecyclerView.Adapter<HistoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.history_item,parent,false)
        return HistoryViewHolder(view)
    }

    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }
}

class HistoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val binding: HistoryItemBinding = HistoryItemBinding.bind(itemView)

    fun bind(item: DayWiseModel) {
        binding.date.text = Helper.parseFormattedDateStr(item.date)
        binding.numCases.text = "Total Cases: ${item.numberOfCases}"
    }
}