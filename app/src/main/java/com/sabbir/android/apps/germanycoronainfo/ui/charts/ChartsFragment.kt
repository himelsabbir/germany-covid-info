package com.sabbir.android.apps.germanycoronainfo.ui.charts

import android.app.AlertDialog
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.sabbir.android.apps.germanycoronainfo.databinding.FragmentChartsBinding
import dmax.dialog.SpotsDialog
import java.util.stream.Collectors


class ChartsFragment : Fragment() {

    private lateinit var chartsViewModel: ChartsViewModel
    private var _binding: FragmentChartsBinding? = null
    private lateinit var barChart: BarChart
    private lateinit var typeFaceOpenSansLight: Typeface
    lateinit var dialog: AlertDialog

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        chartsViewModel =
            ViewModelProvider(this).get(ChartsViewModel::class.java)

        _binding = FragmentChartsBinding.inflate(inflater, container, false)
        val root: View = binding.root
        typeFaceOpenSansLight = Typeface.createFromAsset(context?.assets, "OpenSans-Light.ttf")
        dialog = SpotsDialog.Builder().setContext(context).build()
        dialog.show()
        //setUpDeathBarChart()
        setUpPieChartDataForTotalData()
        return root
    }

    override fun onResume() {
        super.onResume()
        //barChart.invalidate()
    }

    private fun setUpPieChartDataForTotalData() {
        binding.pieChartTotal.description.isEnabled = false
        binding.pieChartTotal.setCenterTextTypeface(typeFaceOpenSansLight)
        binding.pieChartTotal.centerText = generateCenterText()
        binding.pieChartTotal.setCenterTextSize(10f)
        binding.pieChartTotal.setCenterTextTypeface(typeFaceOpenSansLight)

        // radius of the center hole in percent of maximum radius
        binding.pieChartTotal.holeRadius = 45f
        binding.pieChartTotal.transparentCircleRadius = 50f

        val l: Legend = binding.pieChartTotal.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)

        chartsViewModel.germanyLatestInfoSummaryObj.observe(viewLifecycleOwner, {
            dialog.dismiss()
            binding.pieChartTotal.data = chartsViewModel.generatePieChartTotalData(it)
            binding.pieChartTotal.invalidate()
        })

    }

    private fun generateCenterText(): SpannableString? {
        val s = SpannableString("Covid-19\nTill today")
        s.setSpan(RelativeSizeSpan(2f), 0, 8, 0)
        s.setSpan(ForegroundColorSpan(Color.GRAY), 8, s.length, 0)
        return s
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}