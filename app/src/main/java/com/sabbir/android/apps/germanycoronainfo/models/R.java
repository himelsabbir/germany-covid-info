
package com.sabbir.android.apps.germanycoronainfo.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class R {

    @SerializedName("value")
    @Expose
    private Double value;
    @SerializedName("date")
    @Expose
    private String date;

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

}
