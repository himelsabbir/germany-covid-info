package com.sabbir.android.apps.germanycoronainfo.ui.others

import android.app.AlertDialog
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.sabbir.android.apps.germanycoronainfo.R
import com.sabbir.android.apps.germanycoronainfo.api.ApiProvider
import com.sabbir.android.apps.germanycoronainfo.api.ApiUtils
import com.sabbir.android.apps.germanycoronainfo.databinding.OtherFragLayoutBinding
import dmax.dialog.SpotsDialog
import kotlinx.coroutines.launch

/**
 * Created by Sabbir Ahmed Himel aka 'bohemianrobot' on 6/22/21 in Dhaka,Bangladesh.
 * @author bohemianrobot
 * sahimelhimu@gmail.com
 * This code is free of any charge or license or anything
 * do whatever you want.
 *
 */

class OthersFragment(): Fragment() {

    private lateinit var binding : OtherFragLayoutBinding
    lateinit var dialog: AlertDialog

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog = SpotsDialog.Builder().setContext(context).build()
        dialog.show()
        lifecycleScope.launch {
            val responseBody = ApiUtils.getApiService().getDistrictIncidentHeatMap()
            if (responseBody!=null){
                try {
                    val image = BitmapFactory.decodeStream(responseBody.byteStream())
                    if (image!=null){
                        dialog.dismiss()
                        binding.heatImage.setImageBitmap(image)
                    }
                }catch (e:Exception){
                    Toast.makeText(requireContext(),"Network error,please try again later",
                        Toast.LENGTH_SHORT).show()
                }

            }
        }
        val view = inflater.inflate(R.layout.other_frag_layout,container,false)
        binding = OtherFragLayoutBinding.bind(view)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        if (dialog.isShowing) dialog.dismiss()
    }
}