package com.sabbir.android.apps.germanycoronainfo.ui.dashboard

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sabbir.android.apps.germanycoronainfo.api.ApiUtils
import com.sabbir.android.apps.germanycoronainfo.models.DayWiseModel
import com.sabbir.android.apps.germanycoronainfo.models.GermanyLatestInfoSummary
import com.sabbir.android.apps.germanycoronainfo.models.HistoryResponseModel
import kotlinx.coroutines.launch

class DashboardViewModel : ViewModel() {

    fun getHistoryData(numOfDays: Int) {
        /*germanyLatestInfoSummary = MutableLiveData<List<DayWiseModel>>().apply {
            viewModelScope.launch {
                value = apiService.getDayWiseCases(numOfDays).caseList
                germanyLatestInfoSummaryObj = germanyLatestInfoSummary
                Log.d("Vmodle", "getHistoryData: ${germanyLatestInfoSummaryObj.value}")
            }
        }*/

        viewModelScope.launch {
            germanyLatestInfoSummary.postValue(apiService.getDayWiseCases(numOfDays).caseList)
        }

    }

    private val apiService = ApiUtils.getApiService()
    var germanyLatestInfoSummary = MutableLiveData<List<DayWiseModel>>().apply {
        viewModelScope.launch {
            value = apiService.getDayWiseCases(7).caseList
        }
    }




    var germanyLatestInfoSummaryObj: LiveData<List<DayWiseModel>> = germanyLatestInfoSummary
}