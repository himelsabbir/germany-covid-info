package com.sabbir.android.apps.germanycoronainfo.utils

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import java.text.ParseException
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Created by Sabbir Ahmed Himel aka 'bohemianrobot' on 6/18/21 in Dhaka,Bangladesh.
 * @author bohemianrobot
 * sahimelhimu@gmail.com
 * This code is free of any charge or license or anything
 * do whatever you want.
 *
 */
object Helper {
    @Throws(ParseException::class)
    fun parseFormattedDateStr(dateStr: String): String {

        val inputFormatter: DateTimeFormatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        val outputFormatter: DateTimeFormatter =
            DateTimeFormatter.ofPattern("EEE, MMM d, ''yy", Locale.ENGLISH)
        val date: LocalDate = LocalDate.parse(dateStr, inputFormatter)
        return outputFormatter.format(date)
    }

    fun parseOnlyDate(dateStr: String):String{
        val inputFormatter: DateTimeFormatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH)
        val outputFormatter: DateTimeFormatter =
            DateTimeFormatter.ofPattern("MMM d", Locale.ENGLISH)
        val date: LocalDate = LocalDate.parse(dateStr, inputFormatter)
        return outputFormatter.format(date)
    }

    fun isConnectedToInternet(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                when {
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                        return true
                    }
                    capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> {
                        Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                        return true
                    }
                }
            }
        }
        return false
    }
}