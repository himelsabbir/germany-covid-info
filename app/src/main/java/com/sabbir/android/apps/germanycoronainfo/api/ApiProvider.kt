package com.sabbir.android.apps.germanycoronainfo.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

/**
 * Created by Sabbir Ahmed Himel aka himelhimu on 6/14/21 in Dhaka,Bangladesh.
 * @author himelhimu
 * sahimelhimu@gmail.com
 * This code is free of any charge or license or anything
 * do whatever you want.
 *
 */
object ApiProvider {

    fun getOkHttpClient(): OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val builder = OkHttpClient.Builder().apply {
            readTimeout(60, TimeUnit.SECONDS)
            addInterceptor(httpLoggingInterceptor)
        }

        return builder.build()
    }

    private fun getInterceptor(): Interceptor {
        return Interceptor() { chain ->
            val original = chain.request()
            val builder = original.newBuilder()
            val request = builder.header("Content-Type", "application/json")
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        }
    }
}